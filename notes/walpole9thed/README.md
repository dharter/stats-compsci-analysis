Reading notes for Walpole, Myers, Myers & Ye textbook:

- Walpole, R. E., Myers, R. H., Myers, S. L., & Ye, K. (2012). 
  [Probability and statistics for engineers and scientists (9th ed)](https://www.amazon.com/Probability-Statistics-Engineers-Scientists-9th/dp/0321629116/ref=sr_1_1?ie=UTF8&qid=1494944975&sr=8-1&keywords=probability+and+statistics+for+engineers+and+scientists). 
  New York: Prentice Hall.

