---
title: 'Lecture: Walpole and Akritas, Chapter 1'
author: "Derek Harter"
date: "May 16, 2017"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo=TRUE, warning=FALSE, message=FALSE, cache=TRUE)
```

## Tidying Data Up

The Example 1.2 from Wal9 Ch1 involves a small set of two samples of data for 
an experiment.  In this hypothetical experiment, we are treating plants
with a nitrogen fertilizer, then measuring the weight of the stems in
grams after the end of 140 days.  The question is: can we tell if there is
an effect (beneficial or otherwise) of the fertilizer treatment to the
seedlings?  This is a classic question of science that we can use statistics
to help us answer.  In this case, from our sampled experimental populations,
can we detect any difference in the treatment vs. no treatment subpopulations,
and if so how certain are we that we are seeing a real difference from the
effect of our experiment.

In R we can create the data set shown in Wal9 Table 1.1 by hand like this:

```{r}
library(tidyverse)
stem.weight.messy <- tribble(
  ~no.nitrogen, ~nitrogen,
  #-----------!-----------
  0.32,        0.26,
  0.53,        0.43,
  0.28,        0.47,
  0.37,        0.49,
  0.47,        0.52,
  0.43,        0.75,
  0.36,        0.79,
  0.42,        0.86,
  0.38,        0.62,
  0.43,        0.46
)
```

```{r echo=FALSE}
library(knitr)
kable(stem.weight.messy)
```

Here I am using the tribble/tibble data structures from the tidyverse package 
which we will talk about later (see R for Data Science).  While it is easy for 
us to understand the layout of this data, each column is a separate condition 
and we list the 10 trials for each of our two conditions in a column, this data
is not *tidy* and thus will be more difficult to analyze in R or many other
stats systems.  When we display tables of data for a paper or presentation we
often might want to violate tidy data principles to make the tables more 
understandable for human readers.  But when working with data for analysis we
want it to be tidy.  In this case, we need every one of the 20 separate trials 
to be in a row of its own.  We would want to then label each tril by the
type of treatment, e.g. `nitrogen` vs. `no.nitrogen`.  We could use some
additional functions from the `tidyverse` package to help us do this (this
is the main purpose of `tidyverse`).  For example, to clean the data up
to a tidy format:

```{r}
# tidy the data
stem.weight <- gather(stem.weight.messy, no.nitrogen, nitrogen, 
                      key='treatment', value='weight') %>% 
               mutate(treatment = as.factor(treatment))
```

```{r echo=FALSE}
kable(stem.weight)
```

Again we will talk more about tidy data concepts elsewhere in this course.
But the point is that we will often need to reformat (munge) the data
we will be using from our textbook and the problems first into a tidy
format before we use it.

## Loading Data

While you can load a data set by hand into an R data frame or a tibble as we 
showed just now, it is almost always easier, even for a dataset as small as the
stem weight data, to just put it into a small plain text file using a simple
format to delimit the data.  For all of these lecture notes, we will be putting
the data into files which can be found in the `data` subdirectory.  We will be
using a simple comma separated value (`csv`) format for the data.  For example,
the stem weight data can be found in a file named 
`data/wal9-ch01-ex1.2-stem-weight.csv`.  Here is how we load a csv file in R
into a data frame:

```{r}
# load the data from csv file
stem.weight <- read.csv('../data/wal9-ch01-ex1.2-stem-weight.csv', header=TRUE)
```

You might want to look at the file now to make sure you understand the format 
and what is happenning.  Also at this point you may want to have worked through
the SC:Repro Lessons 1-5 (up through Exploring Data Frames).  In particular 
I would usually want to double check that stem.weight was loaded correctly,
see how many rows of data it has, and see what the features are (the
columns) and what the data types are of the features, and that they make
sense.  We can start with the `str()` function, which doesn't mean
string but actually means give a compact summary of the structure, 
and the `summary()` function which gives a summary of the data in a data
frame object:

```{r}
str(stem.weight)
summary(stem.weight)
```

As we can see, the stem.weight is a `data.frame` object and it consists of
20 observations, as we expected.  There are 2 variables, or features or
measurements, of this data, the `condition` and the `weight`.  Also, 
very important, `condition` was read in as a `Factor` type (not as
a character string), which is good.  R recognized that the condition
was a finite discrete data type with 2 different factors "nitrogen" and 
"no.nitrogen".  In the summary we can see that 10 of the values were
in the nitrogen condition and 10 in the no.nitrogen condition.  We also
get some information about the distribution of the numeric weight feature,
its minimum and maximum value, and mean and median among other information.

## Basic Plotting in R

The Wal9 Example 1.2 starts out with a basic dot plot of the stem weight
data in an effort to begin to try and answer the research question (does
the treatment cause a measurable difference in growth of the seedlings).
Here is how we can reproduce the dot plot shown in R:

```{r fig.height=2}
# 1d scatter plot by experimental condition
library(ggplot2)
ggplot(stem.weight, aes(x=weight, fill=treatment)) + 
  geom_dotplot(stackgroups=TRUE, method="histodot") +
  scale_x_continuous(breaks=seq(0.0, 1.0, 0.05)) + 
  xlab('stem weight (grams)') + 
  ylab('') + 
  scale_y_continuous(NULL, breaks=NULL)
```

As discussed in Wal9 Example 1.2, notice that the dot plot suggests
that the nitrogen treatment might be having an effect, at least many
of the seedlings treated with nitrogen weight more when measures (the top
4 weights are all from nitrogen treated seedlings).  Likewise the bottom 
6 weights (except for 1) are from the seedlings with no nitrogen treatment.
Could that lowest nitrogen treatment result be an outlier or the result of
a mistake in measurement or otherwise?  But even with that value, by visual
inspection it does appear that the nitrogen treatment plants do weigh more.
But how do we quantify this?

## Simple Summary Statistics: Measures of Location

You are probably already familiar with the concept of the mean value of
some data, and in general how to calculate it.  You should understand
the mean value on an intuitive level.  For example, look at the previous
dot plot.  Imagine looking at only 1 treatment condition, say the nitrogen
treated seedlings.  Imagine the red balls are all 1 pound weights that
we are balancing on a board at the indicated positions.  Where would you
locate a balance point so that the board balances (does not tip down
to the right or left).  In physics this is known as locating the center of
mass of the weights in order to balance the balance board.  By visual 
inspection this center of mass for the red nitrogen balls probably somewhere
around 0.55 wouldn't you say?  Likewise if I had to choose a center of
mass for the blue no.nitrogen treatment it looks like I would start
somewhere around 0.4.  The location of the central tendency of 
a dataset is what is measured by the mean of the dataset.

Later on we will be very concerned with trying to estimate the true mean value
of a population from a sample of that population.  In Wal9 Ch 1 we are given
a formal procedure for how we can calculate the mean of a sample of data,
the **sample mean**. As I said you are probably already familiar with how to
calculate a mean.  Formally, we calculate the sample mean as:

**Definition 1.1**
$$
\bar{x} = \sum_{i=1}^n \frac{x_i}{n} = \frac{x_1 + x_2 + \cdots + x_n}{n}
$$

The greek Sigma $\Sigma$ notation, if you are not familiar with it is simply
shorthand for the summation shown on the right.  E.g. sum up all of the
values in our sample, then divide by n.

This algorithm or procedure is easy to code in any programming language so
a computer will do it for you.  For example, in R we can do the following:

```{r}
# extract only the no.nitrogen values
no.nitrogen <- stem.weight$weight[stem.weight$treatment == 'no.nitrogen']

# sum up all of the values using a loop
s <- 0.0
for (weight in no.nitrogen) {
  s <- s + weight
}

# divide by the number of samples to get the mean
no.nitrogen.mean <- s / length(no.nitrogen)

no.nitrogen.mean
```

As a slightly shorter implementation, there is a function that will sum up
a vector of values which we could have used insted of creating an explicit
loop to sum up all of our sampled values:

```{r}
no.nitrogen.mean <- sum(no.nitrogen) / length(no.nitrogen)
no.nitrogen.mean
```

But in R, unlike other lower level languages, there is built in support
for calculating many of the statistical properties we will use in this
class, including the mean:

```{r}
no.nitrogen.mean <- mean(no.nitrogen)
no.nitrogen.mean
```

In this class we will not be implementing algorithmic features to calculate
statistics like this by hand.  R has a rich set of built in statistical
functions, and we will usually be learning how to use the provided
libraries in order to perform our statistical analysis.

We could have used any of the previous procedures to calculate the
mean of the nitrogen treatment samples.  If we did we would have found
that the mean for the nitrogen samples were:

```{r}
mean(stem.weight$weight[stem.weight$treatment == 'nitrogen'])
```

Did your estimates of the central tendency of the two treatment conditions
match the calculated mean we just got?

## Simple Summary Statistics: Median

There are other measures of central tendency than the mean.  One problem
with the mean is it can be overly impacted by huge outliers.  For example, if
the low value in the nitrogen treatment is a mistake and we removed it, the
mean for the nitrogen would increase quite a bit, up to above 0.60.  Or
as another example, imagine calculating the mean income of owner and servants
who live on a rich land owners estate.  Perhaps we have the following
incomes for 6 servants and the owner (in 1000s of dollars):

```{r}
income <- c(10, 12, 15, 16, 18, 25, 3000) # owner has an income of 3 million / year

mean(income)
```

Here we see that the mean income of the people residing on the estate is
above $400k, but this seems a very skewed summary of the incomes of the people
being measured.  Later on we will see it is important to look at the
distribution of values in a population to understand the properties
of that population.  But for now we can introduce the median  The median
value is simply the value in a sample or population that is in the middle
of all of the other values.  In order to calculate the median we have to first
sort the values from lowest to highest (the incomes were already sorted
in this manner above).  In our case for the incomes we can directly see that
the median value is $16k as there are 3 incomes lower than it and 3
incomes higher.

```{r}
median(income)
```

But there is one small technical difficulty with calculating the median.
What if we have an even number of values?  In that case there is not "middle"
value.  When we have an even number of values, we simple take the two
values that are in the middle of the data set and take an average of those
two values to determine the median.  For example, for the nitrogen treatment
sample population there are 10 values:

```{r}
nitrogen <- stem.weight$weight[stem.weight$treatment == 'nitrogen']
sort(nitrogen)
```

Here we can see that the two middle values, the 5th and 6th, are 0.49 and
0.52 respectively giving a calculated median of 0.505.  We can implement
the calculation of the median in R like this (this will probably be our
last example of implementing a procedure by hand):

```{r}
# need to have values ordered from smallest to largest
# here we cheat, in a language with no sorting library, we would of course
# first have to implement some efficient sorting function
nitrogen <- sort(nitrogen)

# we already know that the nitrogen sample has an even number of values,
# here we calculate the index of the middle 2 values
i1 = length(nitrogen) / 2
i2 = i1 + 1
nitrogen.median <- (nitrogen[i1] + nitrogen[i2]) / 2
nitrogen.median
```

We cheated here in 2 ways.  First of all, as mentioned in the comments, we are
relying on a sort function from R to order the values for us.  Also we have
assumed above that there are an even number of samples.  To write a full
procedure we would first have to check whether the number of samples was
even or odd, and if even we would do the above, but if odd we would simply
find the index of the middle item and return it.

Formally we define the **sample median** as:

**Definition 1.2**
  $$
  \tilde{x} = \begin{cases}
    x_{(n+1)/2},                         & \text{if } n \text{ is odd,} \\\\
    \frac{1}{2} (x_{n/2} + x_{n/2 + 1}), & \text{if } n \text{ is even.}
  \end{cases}
  $$

## Other Measures of Location

From the defintion of the median it is easy to understand other concepts like
**trimmed means** and **percentiles** and **quantiles**.  All three of these
first require us to order the data from smallest to largest as we had to
do in order to calculate the median.  For the trimed mean, for example
if we want to trim away both the smallest and largest 10% of the data, we 
eliminate the smallest and largest 10% of the values and calculate the mean
on the remaining values.  Trimming values, like the median, has the effect
of reducing the influence of extreme or outlier values.  In R we can calculate
trimmed means by passing a trim parameter to the `mean` function:

```{r}
# first show an example of calculating a 10% trimmed mean by hand
# here we only extract indexes from 2 to 9 of the nitrogen sample, thus
# trimming the lowest 10% and highest 10% of the values
mean(nitrogen[2:9])

# or better, let R calculate which values to trim for us
mean(nitrogen, trim=0.1)
```

Percentiles are similar to calculating the median.  In fact the median
values is the value at the 50th percentile.  But we can ask for the 
value at say the 25th percentile, this is simply the value where 25% of the
values are smaller than it and 75% of the values are greater than it.  The
25th percentile is also known as the 1st quantile (where you can also ask for
the 2nd quantile which is the mean or the 3rd quantile with is the 75th
percentile).  As with the mean, if there is no exact value at the desired
percentile index, we take a weighted average of the values above and below
this location to extrapolate the percentile.  In R we use the quantile
function.  By default it returns the 25th, 50th and 75th percentile (the
quantiles), but we can use it to calculate and return any percentile we want:

```{r}
quantile(nitrogen)

# get the 10th percentile and the 85th percentile
quantile(nitrogen, c(0.1, 0.85))
```

Notice that the 0th percentile is actually the minimum value (no values are less
than it) and the 100th percentile is the maximum value.  These are the values
that are returned by the summary function when a column or feature in a
data table is numeric (though note of course the symmary of the weight is
a summary over all 20 sampled weights from both treatment conditions):

```{r}
summary(stem.weight)

summary(nitrogen)
```

Looking at quantiles and the mean and median is a way to begin to understand
the distribution of a set of data.  But these are still summary
statistics of the spread of the data.  There is a more direct way of
summarizing the amount of spread or variability of a sample of data, and
that leads us to the sample variance and standard deviation.

## Measures of Variability: Sample Variance and Standard Deviation

Sample variability plays an important role in data analysis.  Something
that has a lot of variability is going to be harder to control and predict
than something that is much more standardized and orderly.  Looking
back at the stem weights data, it looks like the variability in the
nitrogen sample is larger than in the no nitrogen treatment condition.  For
example, the nitrogen sampled population contained both the smallest
and largest value (all of the no nitrogen treatment values fell within
the extremes of the weights we saw from the treated population).  Perhaps
there is something about using the nitrogen fertilizer that not only 
increases the stem weight, but makes the growth more variable so that we
are likely to see some small seedings as well (e.g. growth is more inconsistent).

This figure from Wal9 pg. 14 is another good example of the importance of
variability.

![Example of Population Variance](../figures/ch01-variability-example.png)

The data sets A an dB have means that are roughly the same.  But looking at
the variability of the data we would intiutively come to completely different
conclusions about the two populations.  In data set B there is no overlap
of any of the measured samples between the populations.  If we saw data
like this we would probably be pretty confident that the means of the
underlying population we were sampling data from were different, e.g.
that the mean of the data for population X is less than the mean of the
data for population O.

But in data set A both of the sampled populations apread out more, and there is
some overlap of the values between the two sampled populations.  It is not
clear in data set A if the mean of the X population is truly less than the 
mean of the O population, or if instead the difference in means was some 
random fluctuations we would expect if we took a sample from populations 
that had high variability but whos true means did not in fact actually differ.

This figure is an example of a very common means hypothesis testing experiment.
If the purpose of drawing the samples was to try and detect if the mean of
the two sampled populations differed or not, then the evidence if we see B
should be much more convincing to us that the means differ, than if we instead
saw the data in set A.

One easy measure of variance is to calculate the range of the values in the
sample.  This is simply the difference between the maximum value and the
minimum value that was sampled.  But much more useful is the 
**sample standard deviation**, which is derived from the **sample variance**:

**Definition 1.3**
**Sample variance** is denoted by $s^2$
$$
s^2 = \sum_{i=1}^{n} \frac{(x_i - \bar{x})^2}{n-1} = \frac{1}{n-1} \sum_{i=1}^n (x_i - \bar{x})^2
$$
  
**Sample standard deviation** is denoted by $s$ (sometimes called sigma or $\sigma$)
$$
s = \sqrt{s^2}
$$

If you look closely at what the variance is calculating you should see that
it is calculating an average (though by dividing by $n - 1$ rather than $n$
which we will get back to in a moment).  But it is calculating
an average of the square of the difference between each measurement in the
population $x_i$ and the mean value of the population $\bar{x}$.  First of
all, by looking at the difference between a value and the average of
the population, we are measuring how close or far it is from the center
of the population, e.g. its spread or variation.  For the variance
we want a summary of the amount of "spread" the values of the population have.
If we just summed these up, some spreads would be negative and some would
be positive.  We really want an average of the magnitude of all of these
spreads.  We could simply take the absolute value of this difference and
takes the average of these.  This is known as the mean absolute difference.
But for reasons we will touch on later it is more useful to take the mean of
the square of the differences, which is how we calculate the variance.  Takeing
the square of this difference will of course make any negative difference
positive, so if we average these squared difference we are again calculating
an average magnitude, but of the squared differences in the case of the 
variance.

The variance contains an average of the squares, so the units of spread it
represents are squared.  Thus by taking the square root we return the units
of measure of the spread back into norml (not squared) units.  Thus the standard
deviation can be interpreted directly as how much spread in the original units
measured the data has.  For example, lets calculate the variance and standard
deviation for the nitrogen and no nitrogen samples:

```{r}
var(nitrogen)
sd(nitrogen)
var(no.nitrogen)
sd(no.nitrogen)
```

First of all, note that the variance (and thus also the standard deviation) for
the nitrogen sampled populations is quite a bit larger than for the no nitrogen
population as we observed from the dot plot.  Also if you want to check, you
should find that indeed the standard deviation is simply the square root of the
calculated variance:

```{r}
sqrt(0.0348722)  # variance of nitrogen
sqrt(0.005298889) # variance of no nitrogen
```

Back to our point about units, the nitrogen samples had a varience of 0.0348
and a standard deviation of 0.1867.  We can interpret the standard deviation
for nitrogen as meaning that the average magnitude of devaiton is about 0.1867
grams of weight in the sample.

The other question about why we divide by $n - 1$ rather than $n$ for the
variance is technical, though our textbook addresses it a bit.  We will later
be using the concept of **degrees of freedom** for other statistical methods.
For now it is sufficient to see that when $n$ is very larger, there will be
little difference between dividing by $n$ or $n - 1$, but if you want to
implement calculating a variance by hand for yourself, you need to divide
by $n - 1$ if you want to get the same answer you will see in statistical
systems and packages like R.

## TODO: Discuss Computational Formula for $s^2$

## Basic Graphs for Data Visualizaiton in R

Lets see how to reproduce most of the simple plots given in Wal9 section 1.6
and in Akr section 1.5.

### Scatter Plots

The scatter plot is similar to the dot plot we have already seen.  In this
data we want to plot two dimensions of informaiton, the Tensile Strength
of a fabric with respect to the percentage of cotton present in the fabric.

```{r, fig.width=6.5}
cloth.data <- read.csv('../data/wal9-ch01-table1.3-tensile-strength.csv', header=TRUE)

library(latex2exp)
ggplot(cloth.data, aes(x=factor(cotton.pct), y=tensile.strength)) +
  geom_dotplot(binaxis='y', fill='lightblue') +
  labs(x='Cotton Percent', y = TeX('Tensile Strength (N/m^2)') )
```

Typically for a scatter plot we want the independent measure to be plotted
on the X axis, and the dependent measure on the Y axis.  In this experiment, we
are trying to determine how the tensile strength of the cloth varies
depending on the percent of cotton present in the cloth.  Thus the experiment
varies the independent measure (the cotton percentage) then measures the effect
this has on the dependeint measure (the resulting tensile strength depends
on the percentage of cotton in the cloth).   For graphs you are presenting,
you should always strive to correctly label all of your axis clearly, indicate
units of measurement where needed, and in general make sure you provide 
enough information on the graph so it can be understood even if you are not
there to explain the figure.

Here is another example of a scatter plot from Akr figure 1.6, plotting Bear
chest girth against the bear weight:

```{r, fig.height=6}
bears.data = read.table('../data/akr-ch01-figure1.6-bears-data.csv', header=TRUE)
plot(bears.data$Chest.G, bears.data$Weight, pch=21, 
     bg=c('light blue', 'black')[unclass(bears.data$Sex)])
legend(x=22, y=400, pch=c(21, 21), 
       pt.bg=c('light blue', 'black'),
       legend=c('Female', 'Male'))
```

As a final example of a scatterplot, the bears data contains many other
measurements.  In a scatter plot we can only plot the relatinship of two
variables against one another at a time.  If we have many variables, we might 
want to systematically plot the relationship of every pair to look for
interesting correlations.  In the base system we can use the bair function
to do this:

```{r, fig.height=6}
pairs(bears.data[4:8], pch=21, bg=c('light blue', 'black')[unclass(bears.data$Sex)])
```

And here is a more complex example for Akr Figure 1-8 showing some of the
power of getting down and programming in R.  Here the author creates a 
function in order to add histograms to the margins of the first bear scatter
plot.  This is useful as it allows us to see the variation of each individual
variable on the plot.

```{r, fig.height=6}
scatterhist = function(x, y, xlab="", ylab=""){
  zones=matrix(c(2,0,1,3), ncol=2, byrow=TRUE)
  layout(zones, widths=c(4/5,1/5), heights=c(1/5,4/5))
  xhist = hist(x, plot=FALSE)
  yhist = hist(y, plot=FALSE)
  top = max(c(xhist$counts, yhist$counts))
  par(mar=c(4,4,1,1))
  #plot(x,y, col="red")
  plot(x,y, pch=21, bg=c("light blue","black")[unclass(bears.data$Sex)]) 
  legend( x=22, y=400,pch = c(21,21), pt.bg = c("light blue","black"),legend = c("Female", "Male"))
  par(mar=c(0,3,1,1))
  barplot(xhist$counts, axes=FALSE, ylim=c(0, top), space=0)
  par(mar=c(3,0,1,1))
  barplot(yhist$counts, axes=FALSE, xlim=c(0, top), space=0, horiz=TRUE)
  par(oma=c(3,3,0,0))
  mtext(xlab, side=1, line=1, outer=TRUE, adj=0, 
    at=.8 * (mean(x) - min(x))/(max(x)-min(x)))
  mtext(ylab, side=2, line=1, outer=TRUE, adj=0, 
    at=(.8 * (mean(y) - min(y))/(max(y) - min(y))))
}

scatterhist(bears.data$Chest.G, bears.data$Weight, 
            xlab="Chest Girth", ylab="Weight")
```

### Stem and Leaf Plots

Stem and leaf plots are really just a cheap non-graphical way in order to get
a histogram like visualization of a set of data.  These were useful in the
days before graphics were available interactively at your computing system
or terminal, but now adays I would simply create histograms rather than
use stem and leaf plots.  But for completeness, here is the command in R
to get the stem and leaf plot of the battery life data:

```{r}
battery.data <- read.csv('../data/wal9-ch01-table1.4-battery-life.csv', header=TRUE)
stem(battery.data$hours)
```

### Histogram

The default histogram function in R will only plot the bars of the histogram
using absolute counts or displayed as a density (which we will discuss more
about in later chapters).  You can get a simple histogram of counts in bins
of the battery life data like this:

```{r}
hist(battery.data$hours, 
          breaks=seq(1.4999, 4.9999, 0.5), 
          freq=TRUE, col='lightblue', 
          xlab='Battery Life (years)', ylab='Frequency',
          plot=TRUE)
```

If you set freq to FALSE you will see that you get the component density of
each of the histogram bars.  The density is 2 times as big as the relative
frequency calculated in the textbook because each bar has a width of 0.5, so
the height of the bar ends up being twice as high as the relative frequency.

If we want to recreate the figure from Wal9 textbook unfortunately we have
to calculate the relative frequencies ourself and build a bargraph by hand.

```{r}
f <- hist(battery.data$hours, 
          breaks=seq(1.4999, 4.9999, 0.5), 
          freq=FALSE, col='blue', 
          xlab='Battery Life (years)', ylab='Relative Frequency',
          plot=FALSE)


f.table <- tibble(class.interval = f$breaks[1:7], class.midpoints=f$mids, 
                  frequency = f$counts, 
                  relative.frequency= f$counts / sum(f$counts))

barplot(f.table$relative.frequency,
        names.arg=f.table$class.midpoints,
        col='lightblue', space=0, yaxp=c(0.0, 0.375, 3),
        xlab='Battery Life (years)', ylab='Relative Frequency')
```

In Ark Figure 1-4 they give another example of a histogram, combined with a
smoothed density line plot of the spread of the data.

```{r}
hist(faithful$eruptions, freq=FALSE,
     main='Eruption Durations of Old Faithful Geyser',
     xlab='Duration (min)', col='light blue')
lines(density(faithful$eruptions), col='blue')
```

In this figure, the bars frequency of eruption durations
in minutes of the old faithful geyser. In this case we plot the bars using the
density calculation.  So for example to interpret this histogram take a look at
the leftmost bar.  This represents the frqeuncy of eruptions that lasted less
than 2 minutes.  The area of the bars in a density plot histogram must add up to
1, so that we can interpret the graph as representing a probability distribution
(we will discuss this much more in later chapters).  The density functions
plotted by the line represents the non binned calculation of this density
frequency for the given data.  Again as we will discuss later, the density
line can be directly  used to calculate the probability of some eruption
duration occurring.  For example, to calculate the probability that the duration
would last less than 2 minutes, you would need to calculate the area under
the density curve from 0 to 2 to get this probability.  The total area under
and probability density curve must add up to 1.0.

Another note, the 'hist' plotting function is from the basic R base plotting
system.  There are actually 3 main plotting systems in R, the base plotting
system, the lattice package, and the ggplot2 package.  I had been using ggplot
previously.  I am showing the next few examples using the base system because
the Akr textbook gives examples using the base system.  You should do the
SWC lesson on creating publication graphics for more information.

### Box Plots (Box and Whisker Plots)

Box and whisker plots are not as familiar to people outside of statistics, but
they are used very commonly in statistics, data science and many scientific
disciplines.  The histogram allows us to get a feel of the spread of our
data.  Likewise so does the box and whisker plot, but in a more summarized
manner.

Here is a basic box and whisker plot of the Wal9 nicotine data.

```{r, fig.width=8}
cigarette.data <- read.csv('../data/wal9-ch01-table1.8-nicotine-data.csv', header=TRUE)
boxplot(cigarette.data$nicotine,
        horizontal=TRUE,
        border='lightblue',
        xlab='Nicotine (units??)')
```

In a boxplot the quartiles are being plotted.  The first and third quartile
location of the data are the bounds of the box, and the 2nd quartile (the median
) is given as the thick line inside of the box.  The whiskers represent the
minimum and maximum of the data, except in most typical boxplots a test is
done for potential outlier data.  You can read the R documentation for the
details, but the basic idea is that any data points that are spread out further
than some measure are instead plotted as the circles you see, indicating
potential outliers.  When an outlier is present, then the whiskers are placed
at the minimum or maximum value that was not labeled as an outlier.  Different
systems might calculate what is considered as an outlier differently (and in 
R you can specify how it should determine which values to plot as outliers).  
It can make a difference sometimes what is being used to determine ourlier
points, but in general it is good enough to understand that the quartiles
are being represented, and some attempt is being made to identify extreme
points and remove them so you can get a feel for the variance of the data
without these extremes.


### 3D Scatter Plots

Humans are only capable of perceiving relative position data in 3 dimensions.
And there is only so much you can do to project a 3D representation onto a
2D surface.  Nonetheless sometimes a 3D projections of data can be useful.
Here we recreate the example 3D scatter plot of the temperature, electricity
and production car data from Ark Figure 1-9.

```{r, fig.height=6}
library(scatterplot3d)
el.data = read.table('../data/akr-ch01-figure1.9-electrprodtemp.txt', header=TRUE)
attach(el.data) # you might see attach used, it allows us to refer to members in df
scatterplot3d(Temperature, Production, Electricity,
              angle=35, col.axis='lightblue', col.grid='lightblue',
              bg='black', main=' ', pch=21, box=TRUE)
```


### Pie Chart

R can do a basic Pie chart.  However Pie charts are not currently considered
very good data visualizations in the data analysis community.  Basically
it is very hard to compare the relative area of sections of a pie chart,
thus other visualizations such as basic bar graphs are usually much more
useful.  Nonetheless here is a basic pie chart in R of the Akr Figure 1-10
light vehicle market share data:

```{r, fig.height=6}
ms.data = read.table('../data/akr-ch01-figure1.10-marketsharelightveh.txt', header=TRUE)
pie(ms.data$Percent, labels=ms.data$Company, col=rainbow(length(ms.data$Percent)))
```

### Bar Chart
A Histogram is a particular type of bar chart where (usually) the x axis is
some continuous value.  When the values are discrete, for example if you
want to visualize the relative amount of market share for light vehicles
for the same data as in the pie chart it is often much better to use a
basic bar chart.  Here is Akr Figure 1-11:

```{r, fig.height=6}
barplot(ms.data$Percent, names.arg=ms.data$Company, col='lightblue', las=2, ylab='Market Share (%)')
```

ttt
