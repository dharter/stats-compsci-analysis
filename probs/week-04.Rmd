---
title: "Chapter 4 Problems"
author: "Derek Harter"
date: "Summer I/II, 2017"
output: 
  pdf_document:
    number_sections: false
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Walpole Chapter 4

\rule{6.5in}{2pt}

## 4.1

```{r}
# expected value
mu.x <- 0.45 * 0 + 0.37 * 1 + 0.16 * 2 + 0.05 * 3 + 0.01 * 4
mu.x
```

$$
E(x) = 0.45 (0) + 0.37 (1) + 0.16 (2) + 0.05 (3) + 0.01 (4) = `r mu.x`
$$


\rule{6.5in}{2pt}

## 4.3

First we need to solve 3.25.  Here is an R solution for finding the distribution
of the random variable $T$, the value of the 3 coins selected from the box. 

```{r}
library(gtools)
# 6 coins in total
f <- combinations(6, 3, 1:6)
f[(f>=1) & (f <= 4)] <- 10 # 4 dimes
f[(f >= 5) & (f <=6)] <- 5 # 2 nickels
colnames(f) <- c('coin.1', 'coin.2', 'coin.3')
coin.space <- data.frame(f)
coin.space$T <- rowSums(coin.space)
coin.space
coin.frequency <- xtabs(~ T, coin.space)
coin.frequency
coin.pmf <- coin.frequency / nrow(coin.space)
coin.pmf

barplot(coin.pmf, xlab='T (total of 3 coins, cents)', ylab='pmf')
```

So now given the pmf in the `coin.pmf` variable, we can easily calculate
the expected value of T

```{r}
# we will do this in a vectorized way
# We get the variable T back out of the pmf,
# need to convert from string index back to a number
T <- as.numeric(dimnames(coin.pmf)$T)

# if we sum up Ttimes the pmf f(x) we get the expected value
mu.T <- sum(T * coin.pmf)
mu.T
```

\rule{6.5in}{2pt}

## 4.5

A fair game is one where you expect to win the same amount as you loose
(if you play long enough).  Or in other words, if you pay exactly the
expected winning, you should break even in the long run.  In general, this
question illustrates using the expected value in games of chance.  If you
can find the PMF or PDF for the game of chance, and use it to calculate the
expected winnings for the game, you will know how much your payment or bet
should be for a fair game, or what amount the house is favored by for 
the game given the odds.

Here we again need to first determine the probability funciton (PMF) for the
game being played.  In this case, the probability is straight forward, we draw 1
card from a deck of 52.  There are 8 jacks or queens in the deck, so there is
an $8 / 52$ chance of winning the \$3 payoff, and likewise there are also 
8 kings or aces so a $8 / 52$ chance of winning the \$5 payoff.  We also need
the odds of not winning (the \$0 payoff).  The complement of a winning payoff
will be $(52 - 16) / 52 = 36 / 52$.  So we have the following PMF

 X      0      3      5
---- ------- ------ ------
f(x)  36/52   8/52   8/52

The expected value is
$$
E(X) = 0 (\frac{36}{52}) + 3 (\frac{8}{52}) + 5 (\frac{8}{52}) = \frac{64}{52} = \$1.2307
$$

So if you pay \$1.23 you would expect (if you play a huge number of games) to end
up winning a bit, and in the long run, if you pay \$1.24 you will loose a bit of money.

Lets run a random simulation to see if this result is correct.  In this
simulation, we can set the amount of money we pay to play each game, and keep
track of the amount of money we get from payoffs.  If you play the game
1,000,000 times, see what happens if you pay \$1.23 vs. \$1.24

```{r, cache=TRUE}
# we make this a function, so that we can reuse and easily compare
# results for different payment/bets
card.game.sim <- function(bet, N=1e6) {
  
  # we start out even
  winnings = 0.00
  for (i in 1:N) {
    # each time we play a game, we have to pay the amount to play
    winnings = winnings - bet
    
    # now we draw a card
    payoff <- 0.00
    card <- sample(1:52, 1) # sample 1 card from deck of 52,
    # we designate cards 1-8 as the $3 payoffs, jack or queen
    # and cards 9-16 as the $5 payoffs, king or ace
    if ( (card >=1) & (card <= 8) ) {
      payoff = 3.00
    }
    else if ( (card >=9) & (card <= 16) ) {
      payoff = 5.00
    }
    
    winnings = winnings + payoff
  }
  
  
  # the result of a simulation of N games is what we won
  winnings
}

# try N simulations paying $1.23 to play
card.game.sim(1.23)

# try N simulations paying $1.24 to play
card.game.sim(1.24)

# should be close to, but  not exactly equal to, 0
card.game.sim(64/52)
```

Even with a million simulated plays, the margin of less than a penny might
not be quite enough to always win a bit when you pay \$1.23 and loose a bit
when you pay a penny more, but it usually does.  


\rule{6.5in}{2pt}

## 4.13

Lets first solve this analytically.  Here we have 2 different PDF functions which
govern the probability of running the vacuum between 0 and 100 hours, and the
probabiliyt of running the vacuum between 100 and 200 hours.  To solve, we
simply need to separately integrate over the different ranges for the
expected value.
$$
E(X) = \int_{-\infty}^{\infty} x f(x) \; dx = \int_0^1 x^2 \; dx + \int_1^2 2x - x^2 \; dx = \frac{1}{3}x^3 \Big{|}_0^1 + [ x^2 - \frac{1}{3}x^3 ] \Big{|}_1^2 = 1
$$
So the expected value is 1 (or 100 hours since the units are expressed in terms of
100s of hours).

Using R to approximate the integration, we get the following result

```{r}
# function to describe the pdf of expected probability of total number of
# hours of vacuum operation. Only tricky part is we need to
# nest the condition for our probability ranges
f <- function(x) {
  ifelse(
    # check if x between 0 and 1
    ((x > 0) & (x < 1)),
    
    # if between 0 and 1
    x,
    
    # otherwise, check if between 1 and 2
    ifelse(
      ( (x >= 1) & (x < 2) ),
      
      # if between 1 and 2
      2 - x,
      
      # otherwise out of range, so evaluates to probability 0
      0
    )
  )
}

f.expected <- function(x) {
  x * f(x)
}

integrate(f.expected, -Inf, Inf)
```

Just for fun, lets visualize the PDF that was defined.
```{r}
# just fyi, what does this pdf look like?
x <- seq(-1, 3, 0.1)
plot(x, f(x), type='l')
```

So, if you remember what the definition of mean or expected
value is, especially the geometric interpretation of the point that centers
the mass of the PDF, obviously the expected value must be 1 for this PDF.


\rule{6.5in}{2pt}

## 4.17

The expected value of a new random variable $g(X)$ this is some function of
the random variable $X$ is simply calculated as (Theorem 4.1):
$$
\mu_{g(X)} = E[g(X)] = \sum_x g(x) f(x)
$$
So we have $g(X) = (2X + 1)^2$ and thus
```{r}
g.exp <- ((-5)**2 * 1/6) + (13**2 * 1/2) + (19**2 * 1/3)
g.exp
```

$$
\mu_{g(X)} = E[g(X)] = \sum_x (2X + 1)^2 f(x) = (-5)^2 \frac{1}{6} + 13^2 \frac{1}{2} + 19^2 \frac{1}{3} = `r g.exp`
$$

\rule{6.5in}{2pt}

## 4.25

We looked at problem 3.51 in the week-03 problems, so if you need a reminder
about how to determine the joint PMF, you can look there for a discussion.

The random variable $X$ was the total number of kings drawn, and $Y$ was
the total number of jacks.  Analytically the join PMF is goverened by
this expression:
$$
f(x,y) = \frac{\binom{4}{x} \binom{4}{y} \binom{4}{3 - (x + y)} }{\binom{12}{3}}
$$

We can create an R matrix to represent the three card PMF:

```{r}
three.cards.pmf <- matrix(0, nrow=4, ncol=4)
rownames(three.cards.pmf) <- c(0, 1, 2, 3)
colnames(three.cards.pmf) <- c(0, 1, 2, 3)

for (x in 0:3) {
  for (y in 0:3) {
    # only valid for choosing 3 cards
    if (x + y <= 3) {
    three.cards.pmf[as.character(y), as.character(x)] <- 
      (choose(4, x) * choose(4, y) * choose(4, 3 - (x + y))) / choose(12, 3)
      
    }
  }
}
three.cards.pmf
```

The questions asks us to calculate the expected value (the mean) of the total
numbre of jacks and kings drawn when we draw 3 cards.  This can be stated
as a new joint random variable that is a function of the original $X$ and $Y$,
$$
g(X, Y) = X + Y
$$
The expected value of this then is simply:
$$
\mu_{g(X,Y)} = E(g(X,Y)) = \sum_x \sum_y g(x,y) f(x,y)
$$

Lets just use a little R code to calculate the expected value from our matrix/table
we created above:

```{r}
# we will use a nested loop, and keep a running sum of our g(x,y) f(x,y) for all
# combinations of x and y
g.exp = 0 
for (x in 0:3) {
  for (y in 0:3) {
    g.exp = g.exp + ( (x + y) * three.cards.pmf[as.character(y), as.character(x)] )
  }
}

# and the result is...
g.exp
```


\rule{6.5in}{2pt}

## 4.35

We are given the discrete PMF

  x        2       3      4       5       6
------  ------  ------  ------  ------  ------
 f(x)    0.01    0.25    0.4     0.3     0.04

Theorem 4.2 calculates the variance using the $E(X^2)$ and the mean
of the random variable $\mu$, so we need those 2 values first.

```{r}
# not all that necessary for this discrete PMF, but for more complicated
# functions it can be useful to create a function to represent the PMF mapping,
# as we have been doing for the continuous functions.  Here is an example where
# we use an R list as a kind of dictionary, that maps the errors as keys, to
# the probabilities of those errors.  This function, as with the discrete cases
# returns a probability for a valid value of the random variable x, or 0 if it is
# invalid
f <- function(x) {
  # set up a mapping from the errors -> probability
  errors.per.100 <- 2:6
  p <- c(0.01, 0.25, 0.4, 0.3, 0.04)
  # create a list that works kinda like a dictionary
  f.pmf <- as.list(p)
  # we use the errors as the keys or names for each probability
  # unfortunately, these have to be represented as strings for R lists/dicts
  names(f.pmf) <- errors.per.100
  
  
  # look up the probability in pmf table
  if (as.character(x) %in% names(f.pmf)) {
    f.pmf[[as.character(x)]]
  }
  else # not a valid random variable outcome, return 0
  {
    0
  }
}

# the valid range of keys are 2 - 6 for this PMF
# show example of using sum and sapply to perform a summation
# of the sample space
x <- 2:6
sum(sapply(x, f))

# ok for the original question, we want to calculate the variance
# of X.  We need the mean of x (mu or E(X)), as well as the expected
# value of E(X^2) to calculate the variance of random variable X using
# Theorem 4.2
f.exp <- function(x) {
  x * f(x)
}
mu.x <- sum(sapply(x, f.exp))
mu.x

f.exp.xsq <- function(x) {
  x * x * f(x)
} 
exp.xsq <- sum(sapply(x, f.exp.xsq))
exp.xsq

# given these values, the variance is E(X^2) - \mu^2
var <- sigmasq <- exp.xsq - mu.x**2
var
```

Just to verify our understanding, given the discrete probability distribution,
we can think of calculating the mean (expected value) and variance in this way.
Assume that we have sampled 100 times from our code sample space, and our sample
exactly follows the PMF distribution given in our table.  In other words, there
was 1 of the 100 samples of code that had 2 errors, 25 of the 100 samples ended
up have 3 errors, etc.  We can use R's built in methods to calculate the mean
and variance on this concrete representation of the PMF like this:

```{r}
# create a concrete representation of the sample space represented by the PMF
S <- c(rep(2, 1), rep(3, 25), rep(4, 40), rep(5, 30), rep(6, 4))
S

# calculate the mean and variance using R builtins on this "Sample Space"
# this should match the mu.x we got using the discrete PMF above
mean(S)

# the built in var() function calculates the sample variance, so it divides 
# by (n-1), so the variance is a little bit bigger
var(S)

# if we adjust by adding 1 more value to the sample space (equal to the mean), 
# you will see that we get the same PMF/population variance we 
# calculated in previous cell
var(c(S, mean(S)))
```

 
\rule{6.5in}{2pt}

## 4.39

Lets solve this using R.  We previously in 4.13 created the following
function to represent the continuous PDF, and used it to find the expected
value of this distribution, which turned out to be 1.
```{r}
# function to describe the pdf of expected probability of total number of
# hours of vacuum operation. Only tricky part is we need to
# nest the condition for our probability ranges
f <- function(x) {
  ifelse(
    # check if x between 0 and 1
    ((x > 0) & (x < 1)),
    
    # if between 0 and 1
    x,
    
    # otherwise, check if between 1 and 2
    ifelse(
      ( (x >= 1) & (x < 2) ),
      
      # if between 1 and 2
      2 - x,
      
      # otherwise out of range, so evaluates to probability 0
      0
    )
  )
}

f.expected <- function(x) {
  x * f(x)
}

mu.x <- integrate(f.expected, -Inf, Inf)$value
mu.x
```

To calculate the variance, we will use the Theorem 4.2 formula.  We have the mean
of our PDF, but we also need to calculate the $E(X^2)$ of this PDF, after which
we can directly compute the variance.

```{r}
# calculate the expected value of X^2
f.expected.xsq <- function(x) {
  x**2 * f(x)
}
exp.xsq <- integrate(f.expected.xsq, -Inf, Inf)$value

# now we can calculate the variance
var <- sigmasq <- exp.xsq - mu.x**2
var
```

\rule{6.5in}{2pt}

## 4.49

We will use the pattern/framework from previous problem to define
a R function for this discrete PMF, and use it to caclualte the variance
and standard deviation.

```{r}
# the PMF for this problem
f <- function(x) {
  # set up a mapping from the errors -> probability
  errors.per.10m <- 0:4
  p <- c(0.41, 0.37, 0.16, 0.05, 0.01)
  # create a list that works kinda like a dictionary
  f.pmf <- as.list(p)
  # we use the errors as the keys or names for each probability
  # unfortunately, these have to be represented as strings for R lists/dicts
  names(f.pmf) <- errors.per.10m
  
  
  # look up the probability in pmf table
  if (as.character(x) %in% names(f.pmf)) {
    f.pmf[[as.character(x)]]
  }
  else # not a valid random variable outcome, return 0
  {
    0
  }
}

# the valid range of keys are 0 - 4 for this PMF
# show example of using sum and sapply to perform a summation
# of the sample space
x <- 0:4
sum(sapply(x, f))

# ok for the original question, we want to calculate the variance
# of X.  We need the mean of x (mu or E(X)), as well as the expected
# value of E(X^2) to calculate the variance of random variable X using
# Theorem 4.2
f.exp <- function(x) {
  x * f(x)
}
mu.x <- sum(sapply(x, f.exp))
mu.x

f.exp.xsq <- function(x) {
  x * x * f(x)
} 
exp.xsq <- sum(sapply(x, f.exp.xsq))
exp.xsq

# given these values, the variance is E(X^2) - \mu^2
var <- sigmasq <- exp.xsq - mu.x**2
var

# this problem asked for the standard deviation of the PMF which
# is simply the square root of the variance
sd <- sqrt(var)
sd
```


\rule{6.5in}{2pt}

## 4.51

We had problem 3.39 in weeks 3 suggested problems, so you can refer to the 
discussion of the week 3 problems for more details.  This was a discrete joint
probability distribution where we were drawing 4 fruit from a basket of 8, and
$X$ was the count of the number of oranges we drew, and $Y$ was the count of the
number of apples.  To calculate the correlation coefficient, we will need
to calculate the covariance first, using Theorem 4.4.  First lets start with
the discrete joint PMF table from 3.39 that we created:
```{r}
library(gtools)
# first we generate all possible combinations (order doesn't matter) 
# of selecting 4 items of fruit from the 8 (there are 70 such combinations total)
f <- combinations(8, 4, as.character(1:8))

# now we convert the sample space into something we can calculate the
# joint probability with, we assign fruits 1-3 to be the 3 oranges,
# 4-5 to be the 2 apples, and 6-8 to tbe the 3 bananas
f[f=='1' | f=='2' | f=='3'] = 'orange'
f[f=='4' | f=='5'] = 'apple'
f[f=='6' | f=='7' | f=='8'] = 'banana'
colnames(f) <- c('fruit.sample.1', 'fruit.sample.2', 'fruit.sample.3', 'fruit.sample.4')
fruit.space <- data.frame(f)
fruit.space$x <- rowSums(fruit.space == 'orange')
fruit.space$y <- rowSums(fruit.space == 'apple')

# x and y for each unique outcome, we simply need to sum these up, but into a table
# and divide by the total number of outcomes to generate the joint PMF that was asked for
fruit.frequency <- xtabs(~ y + x, fruit.space)
fruit.joint.pmf <- fruit.frequency / nrow(fruit.space)
fruit.joint.pmf
```

To begin, we need the expected values $\mu_X = E(X)$ and $\mu_Y = E(Y)$ of the
random variables individually from our joint distribution.  Lets define 
marginal distribution functions for $X$ and $Y$ and use them to calculate
the needed expected values.
```{r}
# wrap the discrete PMF table in a function, so we can use more easily
# x can range from 0 to 3, but y only ranges from 0 to 2 here
f <- function(x, y) {
  if ((x >= 0) & (x <= 3) & (y >= 0) & (y <= 2) ) {
    # again the table in R is indexed starting at 1, but our PMF uses indexes from 0
    fruit.joint.pmf[y+1, x+1]
  }
  else {
    0
  }
}

f(0, 0)
f(2,1)

# create functions for the marginal distributions
g <- function(x) {
  y <- 0:2
  sum(sapply(y, function(y) f(x, y)))
}

h <- function(y) {
  x <- 0:3
  sum(sapply(x, function(x) f(x, y)))
}

# given the marginal distributions, we can
# calculated the expected values we need
exp.x <- function(x) {
  x * g(x)
}
x <- 0:3
mu.x <- sum(sapply(x, exp.x))
mu.x

exp.y <- function(y) {
  y * h(y)
}
y <- 0:2
mu.y <- sum(sapply(y, exp.y))
mu.y
```

And we also need the expected value of the product $E(XY)$ in order to calculate
the covariance.

```{r}
exp.xy <- function(x, y) {
  x * y * f(x, y)
}

mu.xy <- sum(sapply(x, function(x) sum(sapply(y, function(y) exp.xy(x, y)))))
mu.xy
```
We now have the values we need in order to calculate the covariance $\sigma_{XY}$
```{r}
cov <- sigmaxy <- mu.xy - mu.x * mu.y
cov
```
A negative covariance.  This is kinda expected in this problem, because if
we draw more oranges we will have less space and will thus draw less apples, 
and vice versa.

Once we have the covariance, we will need some further information in order
to make the result scale free in order to calculate the correlation coefficient
$\rho_{XY}$.  By Definition 4.5 we will need the variance of our random 
variables $X$ and $Y$.
```{r}
# to calculate variance, we need the expected values or means of X and Y, which
# we have already calculated, and we also need E(X^2) and E(Y^2) of our random
# variables
exp.xsq <- function(x) {
  x**2 * g(x)
}
x <- 0:3
mu.xsq <- sum(sapply(x, exp.xsq))
mu.xsq

exp.ysq <- function(y) {
  y**2 * h(y)
}
y <- 0:2
mu.ysq <- sum(sapply(y, exp.ysq))
mu.ysq

# calculate the variance \sigma^2
var.x <- sigmasq.x <- mu.xsq - mu.x**2
var.y <- sigmasq.y <- mu.ysq - mu.y**2

# finally we can get the standard deviation sigma
sd.x <- sigma.x <- sqrt(sigmasq.x)
sd.x
sd.y <- sigma.y <- sqrt(sigmasq.y)
sd.y
```

We have calculated everything we need in order to compute the correlation
coefficient.
```{r}
corr.coef <- rho.xy <- sigmaxy / (sigma.x * sigma.y)
corr.coef
```
So the correlation is negative, and after scaling the variables, we see that it
is close to half way between 0 and -1 correlation.  If there were not
another fruit in the basket, we would expect the negative correlation
to be close to 1.  But because there are bananas that can sometimes
be drawn instead of the apples or oranges, the negative correlation is
significant, but less than 1.
 
\rule{6.5in}{2pt}

## 4.53

Lets reuse the R function defining the PMF from problem 4.35, and then we
will define a new random variable which is a function of the existing random
varible X, and calculate the expected value and variance on this new
random variable.

```{r}
# the PMF for problem 4.35, probability of given number of errors per 
# 100 lines of code in some code base
f <- function(x) {
  # set up a mapping from the errors -> probability
  errors.per.100 <- 2:6
  p <- c(0.01, 0.25, 0.4, 0.3, 0.04)
  # create a list that works kinda like a dictionary
  f.pmf <- as.list(p)
  # we use the errors as the keys or names for each probability
  # unfortunately, these have to be represented as strings for R lists/dicts
  names(f.pmf) <- errors.per.100
  
  
  # look up the probability in pmf table
  if (as.character(x) %in% names(f.pmf)) {
    f.pmf[[as.character(x)]]
  }
  else # not a valid random variable outcome, return 0
  {
    0
  }
}

# Z = g(X) = 3X -  2 is our new random variable,
# calculate the expected value (the mean) of the Z = g(x)
g <- function(x) {
  3 * x - 2
}
g.exp <- function(x) {
  g(x) * f(x)
}

x <- 2:6
mu.z <- sum(sapply(x, g.exp))
mu.z

# now calculate the expected value of E(Z^2)
g.exp.xsq <- function(x) {
  g(x)**2 * f(x)
}
exp.gxsq <- sum(sapply(x, g.exp.xsq))
exp.gxsq

# calculate the variance now using mu and E(X^2)
var <- sigmasq <- exp.gxsq - mu.z**2
var
```

However there is a much simpler way to calculate these by noting that the
variable Z is a linear combinary of the random variable X. By Theorem
4.5:

$$
E(3X - 2) = 3 E(X) - 2 = 3 \cdot 4.11 -2 = 10.33
$$

Likewise, we can use Corollary 4.6 of Theorem 4.9 to calculate the variance of the new random 
variable Z, which as we noted is a linear combination of X
$$
\sigma_{3X - 2}^2 = 3^2 \sigma_X^2 = 9 \cdot 0.7379 = 6.6411
$$
\rule{6.5in}{2pt}

## 4.55

We are given the discrete PMF for the probability of random varible X that
the store will sellfrom 0 to 5 cartons of milk.  We are interested in a
different random variable, lets call it Z, which is the expected profit
(or maybe loss?) we can expect to receive from selling this milk.  It
is clear that the variable Z is a function of X.  The profit will depend on
the number of cartons of milk sold, like this:
$$
Z = g(X) = (1.65 - 1.20) x - \frac{1}{4} 1.20 (5 - x)
$$
In english, we receive \$0.45 in profit for every carton of milk we sell, and
for unsold cartons we get back $3/4$ of what we paid for them wholesale, which
translates to having a loss of 1/4 of the wholesale price for each unsold
carton.

Given the PMF probability of selling the milk, and the random variable Z which
tells us the profit/loss for the cartons we manage to sell, we can easily calculate
the expected value $E(Z)$

```{r}
# the PMF for this problem
f <- function(x) {
  # set up a mapping from the errors -> probability
  milk.sold <- 0:5
  p <- c(1/15, 2/15, 2/15, 3/15, 4/15, 3/15)
  # create a list that works kinda like a dictionary
  f.pmf <- as.list(p)
  # we use the errors as the keys or names for each probability
  # unfortunately, these have to be represented as strings for R lists/dicts
  names(f.pmf) <- milk.sold
  
  
  # look up the probability in pmf table
  if (as.character(x) %in% names(f.pmf)) {
    f.pmf[[as.character(x)]]
  }
  else # not a valid random variable outcome, return 0
  {
    0
  }
}

# the valid range of keys are 0 - 5 for this PMF
# show example of using sum and sapply to perform a summation
# of the sample space
x <- 0:5
sum(sapply(x, f))

# ok we want the expected value for Z, the function g(x) of the random
# variable X
g <- function(x) {
  (1.65 - 1.20) * x - 1/4 * 1.20 * (5 - x)
}

g.exp <- function(x) {
  g(x) * f(x)
}

mu.z <- sum(sapply(x, g.exp))
mu.z

```

$$
\mu_{Z} = E(Z) = \sum_x g(X) f(X) = `r mu.z`
$$
So the expected profit is \$0.80 per batch purchased.

\rule{6.5in}{2pt}

## 4.79

You don't really need to be able to come up with this whole formal proof yourself
for this class, but I hope you will be able to intuitively understand the
following.  One way to approach an understanding of Chebhyshev's theorem
is to look at the definition of the variance and standard deviation again.
Recall that the variance $\sigma^2$ is the expected value of the squared
difference between $X$ and the population mean $\mu$
$$
\sigma^2 = E[(X - \mu)^2]
$$

Chebyshev's theorem is a statement about the probability distribution being
within k standard deviations ($k \sigma$) of the population mean.  So to think
about this, break up the interval of our $X$ random variable into 2 separate
intervals, those points that are within $k\sigma$ of the population mean, 
$$
I_1 = \{x_i \; | \;\; |x_i - \mu| < k \sigma \}
$$
(e.g. the points $x_i$ such that the difference is within k standard deviations
of $\mu$), and those points that are further away than $k \sigma$,
$$
I_2 = \{x_i \; | \;\; |x_i - \mu| \ge k \sigma \}
$$
(e.g. the points $x_i$ such that their difference are further than $k \sigma$
from $\mu$).

Given these 2 intervals, it should be clear that we can rewrite the formula for
the variance as
$$
\sigma^2 = E[(X - \mu)^2] = \sum_x (x - \mu)^2 f(x) = \sum_{x_i \in I_1} (x_i -u)^2 f(x_i) + \sum_{x_i \in I_2}(x_i - \mu)^2 f(x_i)
$$
We are using a summation here, treating the probability function as a discrete
PMF, but the exact same ideas work using integration over a continuous PDF.
Here since we split the interval into two intervals $I_1$, $I_2$, we have
also split up the summation to sum up the expected values over these
two different intervals separately.

Since the right hand side is the sum of the expectation within $k \sigma$ of
the mean, plus the sum of the expectation outside further away than $k \sigma$
from the mean, it should be trivial to see that these two sums together always have
to be greater than or equal to either one alone.  So we have
$$
\sum_{x_i \in I_1} (x_i -u)^2 f(x_i) + \sum_{x_i \in I_2}(x_i - \mu)^2 f(x_i) \ge \sum_{x_i \in I_2}(x_i - \mu)^2 f(x_i) 
$$
Again this last inequality is not stating anything surprising.  The sum of
the expectation over both intervals $I_1$ and $I_2$ has to be greater than the 
sum of the expectation over just $I_2$ alone.  Now here is where the trickiest
bit comes in conceptually.  On the right hand side, all of the values of $x_i$ in
the second interval $I_2$ are values that are at least as far, if not further
than $k \sigma$ from the mean.  So if you replace $(x_i - \mu)^2 = (k \sigma)^2$
again the inequality holds, because the original summation must have been bigger
than the summation of $(k \sigma)^2$.  Since the value $(k \sigma)^2$ no longer
depends on $x_i$, we can move it out of the summation and simply multiply
the remaining sum by that value, giving us
$$
\sum_{x_i \in I_2}(x_i - \mu)^2 f(x_i) \ge k^2 \sigma^2 \sum_{x_i \in I_2} f(x_i)
$$
Now the summation on the right hand side is only over the points in the interval
$I_2$, that is the points that are further away than $k \sigma$ from the mean.
Summing up the probability function for all of these points is the same as computing
the probability that a point is further than $k \sigma$ from the mean, e.g.
$P(|X - \mu| \ge k \sigma)$.  Thus we have
$$
k^2 \sigma^2 \sum_{x_i \in I_2} f(x_i) = k^2 \sigma^2 P(|X - \mu| \ge k \sigma)
$$
This whole chain of substitutions started from calculating the variance $\sigma^2$
so we now have
$$
\sigma^2 \ge k^2 \sigma^2 P(|X - \mu| \ge k \sigma)
$$
Which, if we divide by $k^2 \sigma^2$ implies (the inequality is still
the same, we have just flipped the left and right sides)
$$
P(|X - \mu| \ge k \sigma) \le \frac{1}{k^2}
$$
This gives us the probability that the value is further away than $k \sigma$ from
the population mean.  So if we want to know the probability it is within
$k \sigma$ of the mean $\mu$ we need the complement of this probability, hence
$$
P(|X - \mu| < k \sigma) \ge 1 - \frac{1}{k^2}
$$
Chebyshev's theorem is a very important result we will use later on when 
developing formal statements on hypothesis testing, among other things.  It
allows us, in this case, to say with absolute certainity what the probability of
a value being within some distance of the population mean must at least be (the
probability will be at least as big as $1 - \frac{1}{k^2}$).  We can make
this statement only knowing the variance (standard deviation) and
population mean of the distribution we are studying, which is kind of amazing.

\rule{6.5in}{2pt}

## 4.91

This is a simple continuous PDF, having the shape

```{r}
f <- function(x) {
  ifelse( 
    (x >= 0) & (x <= 1),
    2 * (1 - x),
    0
  )
}

x <- seq(-0.5, 1.5, 0.001)
plot(x, f(x), type='l')
```

It should be obvious that this is a valid PDF (the area is 1).  The PDF shows
that the dealer is much more likely to make small profits (close to \$0) and
it is very unlikely they make 1 whole unit (\$5000) of profit on a car.  Also
you could probably work out that the expected value is at $x = 1/3$ (e.g.
half of the probability area is below this point, and half above).

Lets find the answers using R.

a. Find the variance.  To find the variance, we need to calculate the 
   expected value $E(X)$ and we will need the expected value $E(X^2)$.
    ```{r}
    # we defined the PDF function in previous cell and will reuse. First
    # calculate the expected values of X and X^2
    f.exp <- function(x) {
      x * f(x)
    }
    mu.x <- integrate(f.exp, 0, 1)$value
    mu.x
    
    f.exp.xsq <- function(x) {
      x**2 * f(x)
    }
    
    exp.xsq <- integrate(f.exp.xsq, 0, 1)$value
    
    # and with these values we can compute the variance
    var.x <- sigmasq <- exp.xsq - mu.x**2
    var.x
    
    # the standard deviation, which we will need for part b, is then
    sigma.x <- sqrt(var.x)
    sigma.x
    ```

b. Demonstrate that Chebyshev's theorem holds for k = 2 with the density
   function above.  In Theorem 4.10, k represents the number of standard
   deviations away from the mean.  The theorem says that the probability
   of the mean being between $\pm 2k$ or 2 standard deviations from the mean
   is
   $$
   P(\mu - k\sigma < X < \mu + k\sigma) \ge P(`r mu.x - 2 * sigma.x` < X < `r mu.x + 2 * sigma.x`) \ge 1 - \frac{1}{4} \ge \frac{3}{4}
   $$
   Notice for one thing that the lower bound on the probability is actually
   below the probability function range of our random variable $X$.  Again it
   is good to keep in mind that Chebyshev's theorem is conservative.  This is
   saying that *at least* $3/4$ of the values lie within the stated range of
   2 standard deviations around the mean.  More values could be in this range, 
   but there will be no less than 75% within 2 standard deviations.  So to
   demonstrate the theory holds, we need to find the probability
   of our PDF in the given range, and make sure it is at least 0.75 or greater.
    ```{r}
    # calculate the probability for our PDF from +/- 2 sd away from the mean
    k <- 2
    lower.bound <- mu.x - k * sigma.x
    upper.bound <- mu.x + k * sigma.x
    integrate(f, lower.bound, upper.bound)
    ```
    So yes indeed, quite a bit more than 0.75 of the probability density of 
    this PDF lies within 2 standard deviations of the mean.
c. What is the probability that the profit exceeds \$500?  If 1 unit is
   a profit of \$5000, then 0.1 units represents a profit of \$500.  So this
   question is simply asking the probability for our PDF for the range from 0.1
   to 1:
    ```{r}
    integrate(f, 0.1, 1)    
    ```

\rule{6.5in}{2pt}

## 4.97

Lets put the PMF density table into R so we can use it to help us calculate
all of these values.

```{r}
# create a matrix to hold the basic mapping from X, Y variable
# to probability/density
p <- c(0.01, 0.01, 0.03, 0.07, 0.01,
       0.03, 0.05, 0.08, 0.03, 0.02,
       0.03, 0.11, 0.15, 0.01, 0.01,
       0.02, 0.07, 0.10, 0.03, 0.01,
       0.01, 0.06, 0.03, 0.01, 0.01)
lights.pmf <- matrix(p, nrow=5, ncol=5, byrow=TRUE)
lights.pmf
sum(lights.pmf)

# wrap the table in a function that implements the joint
# probability function and that checks bounds on X and Y
# random variables
f <- function(x1, x2) {
  
  if ( (x1 >= 0) & (x1 <= 4) & (x2 >= 0) & (x2 <= 4) ) {
    # because R uses 1 based indexing, and our table starts at 0, we simply
    # add 1 to each index to compensate for the indexing beginning shift
    lights.pmf[x1+1,x2+1]
  }
  else {
    0
  }
}

# again double check our function, and give example of doing a double summation
# for a discrete joint PMF
x1 <- 0:4
x2 <- 0:4
sum(sapply(x1, function(x1) sum(sapply(x2, function(x2) f(x1, x2)))))
```

a. Give the marginal density of $X_1$
b. Give the marginal density of $X_2$
    ```{r}
    # the marginal densities of X_1 and X_2 are the sums of the rows and
    # columns respectively
    # g(x1) - marginal densities with respect to x1
    g <- sapply(x1, function(x1) sum(sapply(x2, function(x2) f(x1, x2))))
    names(g) <- x1
    g
    
    # h(x2) - marginal densities with respect to x2
    h <- sapply(x2, function(x2) sum(sapply(x1, function(x1) f(x1, x2))))
    names(h) <- x2
    h
    ```
c. Give the conditional density distribution $P(X_1 | X_2 = 3)$.  Intiutively
   this condition corresponds to our joint PMF table column where $X_2 = 3$.
   If we divide these values by the sum of that column, we will get the 
   probabilities of $X_1$ given the condition $X_2 = 3$.  Back in chapter 3
   we had the formulat for the conditional probability using the marginal
   distributions (Definition 3.11)
   $$
   f(x_1 | x_2 = 3) = \frac{f(x_1, x_2=3)}{h(x_2 = 3)}
   $$
   We can use our R matrix/table to compute the conditional probability
   like this
    ```{r}
    # f(x1, x2=3) is basically the column where x2 = 3
    sapply(x1, function(x1) f(x1, 3))
    
    # h is a list, h['3'] will get us the marginal distribution where x2 = 3
    h['3']
    
    # so the conditional probability asked for is
    sapply(x1, function(x1) f(x1, 3)) / h['3']
    ```
d. Give the expected value $E(X_1)$
e. Give the expected value $E(X_2)$.  The expected value of $X_1$ and $X_2$ are
   simply the mean value where we ignore the effect of the other joint
   random variable.  Thus, for example, $E(X_1) = \sum_{x_1} x_1 g(x_1)$
    ```{r}
    # calculate expected value (mean) of x_1
    exp.x1 <- function(x1) {
      x1 * g[as.character(x1)]
    } 
    mu.x1 <- sum(sapply(x1, exp.x1))
    mu.x1
    
    # calculate expected value (mean) of x_2
    exp.x2 <- function(x2) {
      x2 * h[as.character(x2)]
    } 
    mu.x2 <- sum(sapply(x2, exp.x2))
    mu.x2
    
    ```
f.  Give $E(X_1 | X_2 = 3)$.  Previously we calculated the conditional
    probability for $X_1$ when $X_2 = 3$.  This basically gives us the
    probability distribution for the conditional probability.  So to calculate
    the expected value (mean) of this conditional probability, we use
    $E(X_1 | X_2 = 3) \sum_{x_1} x_1 P(X_1 | x_2=3)$
    ```{r}
    # lets make a function of the conditional probability we need
    f.cond.x2is3 <- function(x1) {
      # do the same calculation we did in c to get a table of the conditional probabilities
      f(x1, 3) / h['3']
    }
    # and a small function to help us calculate the expected value
    f.exp.cond.x2is3 <- function(x1) {
      x1 * f.cond.x2is3(x1)
    }
    mu.cond.x2is3 <- sum(sapply(x1, f.exp.cond.x2is3))
    mu.cond.x2is3
    ```
g. Give the standard deviation of $X_1$.  We will have to calculate the
   variance of the random variable $X_1$.  We already calculated the
   expected value $E(X_1)$ using the marginal distribution, which we will
   need.  We also need to calculate the expected value $E(X_1^2)$, which
   we will again use the marginal distribution of $X_1$ to do this.
    ```{r}
    # we already have mu.x1 from part 2 calculated.  Lets do something
    # similar to calculate E(X_1^2)
    exp.x1sq <- function(x1) {
      x1**2 * g[as.character(x1)]
    }
    mu.x1sq <- sum(sapply(x1, exp.x1sq))
    mu.x1sq
    
    # now we can calculate the variance and then the asked for standard
    # deviation of X_1
    var <- sigmasq <- mu.x1sq - mu.x1**2
    var
    sd <- sigma <- sqrt(var)
    sd
    ```
